
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover();
            $('.carousel').carousel({
                interval: 2000
            })
        })

        $('#modal-1').on('show.bs.modal', function(e){
            console.log('modal-1 se esta mostrando');

            $('#promociones').removeClass('btn-primary');
            $('#promociones').addClass('btn-dark');
            $('#promociones').prop('disabled', true);

        });

        $('#modal-1').on('hide.bs.modal', function(e){
            console.log('modal-1 se esta ocultando');
        });

        $('#modal-1').on('shown.bs.modal', function(e){
            console.log('modal-1 se mostro');
        });

        $('#modal-1').on('hidden.bs.modal', function(e){

            console.log('modal-1 se oculto');

            $('#promociones').removeClass('btn-dark');
            $('#promociones').addClass('btn-primary');
            $('#promociones').prop('disabled', false);

        });

    